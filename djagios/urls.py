"""djagios URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin

from data_collector import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.StatusView.as_view(), name='status'),
    url(r'^alerts/$', views.AlertListView.as_view(), name='alerts_list'),
    url(r'^alerts/new/$', views.NewAlertView.as_view(), name='alerts_new'),
    url(r'^alerts/(?P<pk>\d+)/edit/$', views.EditAlertView.as_view(), name='alerts_edit'),
    url(r'^alerts/(?P<pk>\d+)/delete/$', views.DeleteAlertView.as_view(), name='alerts_delete'),
]
